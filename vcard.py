import pandas as pd
import sys

output = '员工通讯录.vcf'

contacts = pd.read_excel('员工通讯录.xlsx', engine="openpyxl",sheet_name=0, skiprows=1)

with open(output, 'w', encoding = 'utf-8') as f:
    #将每一行转为字典
    for contact in contacts.to_dict("records"):
        name = str(contact["姓名"])
        # 跳过空行
        if name == "nan":
            continue
        f.write("BEGIN:VCARD\n")
        f.write("VERSION:3.0\n")
        f.write("N:" + name[0] + ";" + name[1:] + ";;;\n")
        f.write("FN:%s\n" % name)
        f.write("ORG;CHARSET=UTF-8:华清远见\n")
        f.write("TITLE:%s\n" % contact["职务"])
        f.write("TEL;TYPE=WORK:%s\n" % contact["联系电话"])
        f.write("EMAIL:%s\n" % contact["邮箱"])
        #f.write("X-QQ:%.0f\n" % contact["QQ"])
        f.write("END:VCARD\n")
