# 办公脚本

#### 介绍
一些办公自动化脚本

#### 使用说明

以下脚本依赖python的pandas和xlrd, openpyxl模块，可以使用以下命令安装
```bash
pip install pandas xlrd openpyxl
```

`curriculum.py` 脚本可以根据Excel格式的课表生成iCalendar日历文件

1.  修改脚本中的name和month变量
2.  执行以下命令生成日历文件
```bash
python curriculum.py 西安中心各班授课安排表2021.1.15.xls
```
3.  脚本会在当前目录下生成扩展名为ics的日历文件，可以导入windows和手机的日历中，也可以导入到日程安排软件中（Outlook）

`vcard.py` 脚本可以根据Excel格式的通讯录生成vcard格式的电子名片文件
1.  执行以下命令生成名片文件
```bash
python vcard.py 员工通讯录.xlsx
```
2.  脚本会在当前目录下生成扩展名为vcf的电子名片文件，可以直接导入手机通讯录
