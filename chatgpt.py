#API: https://platform.openai.com/docs/api-reference/chat

import requests

apikey = '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'

url = 'https://api.openai.com/v1/chat/completions'

headers = {
    'Authorization': 'Bearer ' + apikey,
    'Content-Type': 'application/json',
}

while True:
    user_input = input('User: ')
    data = {
        'model': 'gpt-3.5-turbo',
        'messages': [{'role': 'user', 'content': user_input}],
    }

    response = requests.post(url, headers=headers, json=data).json()

    print("ChatGPT: " + response["choices"][0]["message"]["content"])
    