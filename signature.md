<!--
pandoc --embed-resources -t html -o signature.html signature.md
-->

**刘煜 | 嵌入式实训副主管**

-------------------

[![华清远见西安中心](signature.jpg)](http://xa.hqyj.com)<br>
公司地址：西安市高新区高新一路16号创业大厦7层<br>
手机号码：18066635882<br>
电话号码：029-88756251<br>
咨询热线：400-611-6270<br>
电子邮件：<liuy_xa@hqyj.com><br>
集团官网：[www.hqyj.com](http://www.hqyj.com)<br>
创客学院：[www.makeru.com.cn](http://www.makeru.com.cn)<br>
研发中心：[www.fsdev.com.cn](http://www.fsdev.com.cn)

[北京](http://bj.hqyj.com)
·
[上海](http://sh.hqyj.com)
·
[深圳](http://sz.hqyj.com)
·
[成都](http://cd.hqyj.com)
·
[南京](http://nj.hqyj.com)
·
[西安](http://xa.hqyj.com)
·
[武汉](http://wh.hqyj.com)
·
[广州](http://gz.hqyj.com)
·
[沈阳](http://sy.hqyj.com)
·
[济南](http://jn.hqyj.com)
·
[重庆](http://cq.hqyj.com)
·
[长沙](http://cs.hqyj.com)
