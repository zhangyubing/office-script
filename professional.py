import requests
import json
import pandas

'''
本科专业目录
http://bmfw.www.gov.cn/jybptgdxxbkzyml/index.html

专业类型：
type 1: 基本专业
type 2: 特设专业
type 3: 新专业
'''

专业列表 = pandas.DataFrame(columns=['专业编码', '专业名称', '门类名称', '学科名称'])
学科列表 = {}


def 获取学科列表():
    global 学科列表
    响应正文 = requests.post('http://bmfw.www.gov.cn/ZFW-AccessPlatform/front/professional.do')
    obj = json.loads(响应正文.text)

    for i in range(len(obj['xueList'])):
        学科名称 = obj['xueList'][i]["name"]
        学科编码 = obj['xueList'][i]["code"]
        学科列表[学科编码] = 学科名称


def 获取专业列表(专业类型):
    global 专业列表
    表单参数 = {'type': 专业类型}
    响应正文 = requests.post('http://bmfw.www.gov.cn/ZFW-AccessPlatform/front/sanjiProfessional.do', data=表单参数)
    obj = json.loads(响应正文.text)

    # 获取学科门类
    for i in range(len(obj["leiList"])):
        门类名称 = obj["leiList"][i]["name"]
        门类编码 = obj["leiList"][i]["code"]
        # 获取当前门类所有专业
        for j in range(len(obj["sijiMap"][门类名称])):
            专业名称 = obj["sijiMap"][门类名称][j]["name"]
            专业编码 = obj["sijiMap"][门类名称][j]["code"]
            if obj["sijiMap"][门类名称][j]["prentcode"] != 门类编码:
                print("门类编码不一致")
            学科名称 = 学科列表[obj["leiList"][i]["prentcode"]]
            专业列表.loc[len(专业列表)] = [专业编码, 专业名称, 门类名称, 学科名称]


if __name__ == "__main__":
    获取学科列表()
    for 专业类型 in range(1, 3):
        获取专业列表(专业类型)

    print(专业列表)
    专业列表.to_excel("普通高等学校本科专业目录.xlsx", index=False)
